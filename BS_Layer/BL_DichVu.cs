﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKS_LINQTOSQL.BS_Layer
{
	public class ChiTietDichVu
	{
		public int MaDV { get; set; }
		public string TenDV { get; set; }
		public float Gia { get; set; }
		public int SoLuong { get; set; }
	}
	class BL_DichVu
	{
		public DataTable DSDichVu()
		{
			QuanLyKhachSanDataContext QLKS = new QuanLyKhachSanDataContext();
			var dvs = from d in QLKS.DichVus select d;

			DataTable dt = new DataTable();
			dt.Columns.Add("Mã dịch vụ");
			dt.Columns.Add("Tên dịch vụ");
			dt.Columns.Add("Giá");
			foreach (var dv in dvs)
			{
				dt.Rows.Add(dv.MaDV, dv.TenDV, dv.Gia);
			}
			return dt;
		}
        public DataTable TimKiemDichVu(string text)
        {

            QuanLyKhachSanDataContext QLKS = new QuanLyKhachSanDataContext();
            var dvs = from d in QLKS.DichVus
                      where d.TenDV.Contains(text) || d.Gia.ToString().Contains(text)
                      select d;

            DataTable dt = new DataTable();
            dt.Columns.Add("Mã dịch vụ");
            dt.Columns.Add("Tên dịch vụ");
            dt.Columns.Add("Giá");
            foreach (var dv in dvs)
            {
                dt.Rows.Add(dv.MaDV, dv.TenDV, dv.Gia);
            }
            return dt;
        }
        public void TaoDichVu(string tendv, float gia)
		{
			QuanLyKhachSanDataContext QLKS = new QuanLyKhachSanDataContext();
			DichVu dv = new DichVu();
			dv.TenDV = tendv;
			dv.Gia = gia;
			QLKS.DichVus.InsertOnSubmit(dv);
			QLKS.SubmitChanges();
		}
		public void SuaDichVu(int madv, string tendv, float gia)
		{
			QuanLyKhachSanDataContext QLKS = new QuanLyKhachSanDataContext();
			var query = (from d in QLKS.DichVus where d.MaDV == madv select d).FirstOrDefault();
			query.TenDV = tendv;
			query.Gia = gia;
			QLKS.SubmitChanges();
		}
		public void XoaDichVu(int madv)
		{
			QuanLyKhachSanDataContext QLKS = new QuanLyKhachSanDataContext();
			var query = (from d in QLKS.DichVus where d.MaDV == madv select d).FirstOrDefault();
			QLKS.DichVus.DeleteOnSubmit(query);
			QLKS.SubmitChanges();
		}
		public static List<ChiTietDichVu> ListDichVu(int madatphong)
		{
			List<ChiTietDichVu> ldv = new List<ChiTietDichVu>();
			QuanLyKhachSanDataContext QLKS = new QuanLyKhachSanDataContext();
			var query = from _dp in QLKS.DatPhongs
						join _ctdp in QLKS.ChiTietDatPhongs
						on _dp.MaDatPhong equals _ctdp.MaDatPhong
						where _dp.MaDatPhong == madatphong
						select _ctdp;
			foreach (var q in query)
			{
				ChiTietDichVu ctdv = new ChiTietDichVu();
				ctdv.MaDV = (int)q.MaDV;
				ctdv.TenDV = q.DichVu.TenDV;
				ctdv.SoLuong = (int)q.SoLuong;
				ctdv.Gia = (float)q.ThanhTien;
				ldv.Add(ctdv);
			}
			return ldv;
		}
	}
}
