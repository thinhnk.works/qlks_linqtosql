﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKS_LINQTOSQL.BS_Layer
{
	class BL_DatPhong
	{
		public DataTable LSDatPhong()
		{
			QuanLyKhachSanDataContext QLKS = new QuanLyKhachSanDataContext();
			var dps = from _dp in QLKS.DatPhongs select _dp;

			DataTable dt = new DataTable();
			if (dps != null)
			{
				dt.Columns.Add("Mã đặt phòng");
				dt.Columns.Add("Tên KH");
				dt.Columns.Add("Mã phòng");
				dt.Columns.Add("Ngày đến");
				dt.Columns.Add("Ngày đi");
				dt.Columns.Add("Tiền cọc");
				dt.Columns.Add("Tổng tiền");
				dt.Columns.Add("Số ngày ở");
			}
			foreach (var dp in dps)
			{
				string d1 = String.Format("{0: dd/MM/yyyy}", dp.NgayDen);
				string d2 = String.Format("{0: dd/MM/yyyy}", dp.NgayDi);
				if (dp.MaKH == null)
				{
					dt.Rows.Add(dp.MaDatPhong, "NULL", dp.MaPhong, d1, d2, dp.TienCoc, dp.TongTien, dp.SoNgayO);
				}
				else
				{
					dt.Rows.Add(dp.MaDatPhong, dp.KhachHang.HoTen, dp.MaPhong, d1, d2, dp.TienCoc, dp.TongTien, dp.SoNgayO);
				}
			}
			return dt;
		}

	}
}
