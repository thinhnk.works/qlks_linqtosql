﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;

namespace QLKS_LINQTOSQL.BL_Layer
{
	class BL_NhanVien
	{
		public bool DangNhap(string email, string password, string chucvu)
		{
			QuanLyKhachSanDataContext QLKS = new QuanLyKhachSanDataContext();
			if (chucvu == "Admin")
			{
				var query = (from nv in QLKS.NhanViens where nv.Email == email & nv.ChucVu == "Admin" select nv).FirstOrDefault();
				if (query != null)
				{
					if (password == query.MatKhau)
					{
						return true;
					}
					return false;
				}
			}
			else
			{
				var query = (from nv in QLKS.NhanViens where nv.Email == email & nv.ChucVu == "NhanVien" select nv).FirstOrDefault();
				if (query != null)
				{
					if (password == query.MatKhau)
					{
						return true;
					}
					return false;
				}
			}
			return false;
		}
		public bool XacThucNhanVien(string Email, string SDT)
		{
			QuanLyKhachSanDataContext QLKS = new QuanLyKhachSanDataContext();
			var query = (from nv in QLKS.NhanViens where nv.Email == Email & nv.SDT == SDT select nv).FirstOrDefault();
			if (query != null)
			{
				return true;
			}
			return false;
		}

		public bool DoiMatKhau(string Email, string MatKhauMoi)
		{
			QuanLyKhachSanDataContext QLKS = new QuanLyKhachSanDataContext();
			var query = (from nv in QLKS.NhanViens where nv.Email == Email select nv).FirstOrDefault();
			if (query != null)
			{
				query.MatKhau = MatKhauMoi;
				QLKS.SubmitChanges();
				return true;
			}
			return false;
		}

		public NhanVien LayNhanVien_Email(string Email)
		{
			QuanLyKhachSanDataContext QLKS = new QuanLyKhachSanDataContext();
			var query = (from nv in QLKS.NhanViens where nv.Email == Email select nv).SingleOrDefault();
			if (query != null)
			{
				return query;
			}
			return null;
		}

		public NhanVien LayNhienVien_MaNV(int manv)
		{
			QuanLyKhachSanDataContext QLKS = new QuanLyKhachSanDataContext();
			var query = (from nv in QLKS.NhanViens where nv.MaNV == manv select nv).SingleOrDefault();
			if (query != null)
			{
				return query;
			}
			return null;
		}

		public void ThemNhanVien(string HoTen, string DiaChi, string Email, string SDT, string MatKhau, string ChucVu)
		{
			QuanLyKhachSanDataContext QLKS = new QuanLyKhachSanDataContext();
			NhanVien nv = new NhanVien();
			nv.HoTen = HoTen;
			nv.DiaChi = DiaChi;
			nv.Email = Email;
			nv.SDT = SDT;
			nv.MatKhau = MatKhau;
			nv.ChucVu = ChucVu;
			QLKS.NhanViens.InsertOnSubmit(nv);
			QLKS.SubmitChanges();
		}

		public void SuaNhanVien(int manv, string HoTen, string DiaChi, string Email, string SDT, string MatKhau, string ChucVu)
		{
			QuanLyKhachSanDataContext QLKS = new QuanLyKhachSanDataContext();
			var query = (from n in QLKS.NhanViens where n.MaNV == manv select n).FirstOrDefault();
			query.HoTen = HoTen;
			query.DiaChi = DiaChi;
			query.Email = Email;
			query.SDT = SDT;
			query.MatKhau = MatKhau;
			query.ChucVu = ChucVu;
			QLKS.SubmitChanges();
		}

		public void XoaNhanVien(int manv)
		{
			QuanLyKhachSanDataContext QLKS = new QuanLyKhachSanDataContext();
			var query = (from n in QLKS.NhanViens where n.MaNV == manv select n).FirstOrDefault();
			QLKS.NhanViens.DeleteOnSubmit(query);
			QLKS.SubmitChanges();
		}

		public DataTable LayDSNhanVien()
		{
			QuanLyKhachSanDataContext QLKS = new QuanLyKhachSanDataContext();
			var nhanviens = from n in QLKS.NhanViens select n;
			DataTable dt = new DataTable();
			dt.Columns.Add("Mã nhân viên");
			dt.Columns.Add("Họ tên");
			dt.Columns.Add("Địa chỉ");
			dt.Columns.Add("SDT");
			dt.Columns.Add("Email");
			dt.Columns.Add("Chức vụ");
			foreach (var nv in nhanviens)
			{
				dt.Rows.Add(nv.MaNV, nv.HoTen, nv.DiaChi, nv.SDT, nv.Email, nv.ChucVu);
			}
			return dt;
		}
        public DataTable TimKiemNhanVien(string text)
        {
            QuanLyKhachSanDataContext QLKS = new QuanLyKhachSanDataContext();
            DataTable dt = new DataTable();
            var nhanviens = from n in QLKS.NhanViens
                            where n.HoTen.Contains(text) || n.DiaChi.Contains(text) || n.Email.Contains(text) || n.SDT.Contains(text) || n.ChucVu.Contains(text)
                            select n;
            dt.Columns.Add("Mã nhân viên");
            dt.Columns.Add("Họ tên");
            dt.Columns.Add("Địa chỉ");
            dt.Columns.Add("SDT");
            dt.Columns.Add("Email");
            dt.Columns.Add("Chức vụ");
            foreach (var nv in nhanviens)
            {
                dt.Rows.Add(nv.MaNV, nv.HoTen, nv.DiaChi, nv.SDT, nv.Email, nv.ChucVu);
            }
            return dt;
        }
    }
}
