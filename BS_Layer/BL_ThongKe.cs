﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKS_LINQTOSQL.BS_Layer
{
	class BL_ThongKe
	{
		public float TongDoanhThu()
		{
			QuanLyKhachSanDataContext QLKS = new QuanLyKhachSanDataContext();
			var hds = from _hd in QLKS.HoaDons select _hd;

			float tongDoanhThu = 0;
			if (hds != null)
			{
				foreach (var hd in hds)
				{
					float tongTien = (float)Convert.ToDouble(hd.TongTien + hd.DatPhong.TienCoc);
					tongDoanhThu += tongTien;
				}
			}
			return tongDoanhThu;
		}
		public float DoanhThu1ThangGanNhat()
		{
			DateTime today = DateTime.UtcNow.Date;
			DateTime lastmonth = today.AddDays(-30);
			// Lấy hóa đơn trong 1 tháng gần nhất
			QuanLyKhachSanDataContext QLKS = new QuanLyKhachSanDataContext();
			var hds = from _hd in QLKS.HoaDons where _hd.NgayLap >= lastmonth select _hd;

			float tongDoanhThu = 0;
			if (hds != null)
			{
				foreach (var hd in hds)
				{
					float tongTien = (float)Convert.ToDouble(hd.TongTien + hd.DatPhong.TienCoc);
					tongDoanhThu += tongTien;
				}
			}
			return tongDoanhThu;
		}
		public Dictionary<string, float> DoanhThuTungPhong()
		{
			Dictionary<string, float> dttp = new Dictionary<string, float>();
			QuanLyKhachSanDataContext QLKS = new QuanLyKhachSanDataContext();

			// Danh sách tên phòng
			List<string> list_tenphong = new List<string>() {
				"T101", "T102", "T103",
				"T104", "T105", "T106",
				"T107", "T108", "T109",
				"V201", "V202", "V203"
			};
			foreach (string tenphong in list_tenphong)
			{
				dttp[tenphong] = 0;
				// Lấy tất cả hóa đơn theo tên phòng
				var hds = from _hd in QLKS.HoaDons where _hd.DatPhong.MaPhong == tenphong select _hd;
				float tongTien = 0;
				if (hds != null)
				{
					foreach (var hd in hds)
					{
						tongTien += (float)Convert.ToDouble(hd.TongTien + hd.DatPhong.TienCoc);
					}
				}
				// Gán tổng doanh thu theo tên phòng
				dttp[tenphong] = tongTien;
			}
			return dttp;
		}
	}
}
